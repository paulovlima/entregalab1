---
title: "1.1.0- Equação do Calor"
date: 2022-09-23T16:32:10-03:00
draft: false
katex: true
markup: 'mmark'
---
No post anterior nós demos alguns exemplos sobre as equações diferencias.

Nesse post falaremos mais sobre a **Equação do Calor**.

Primeiro vamos relembrar a equação:
$$ \frac{\partial u}{\partial t} =  a^2 \frac{\partial^2 u}{\partial x^2} \qquad \qquad (1)$$
$$ u(x,0) = u_0(x) \qquad \qquad (2)$$
$$u(0,t) = 0 \qquad \qquad u(L,t) = 0  \qquad \qquad (3)$$

Para resolvermos esse problema nós vamos usar a técnica da separação das variáveis, que satisfaça as condições (1), (2) e (3) e que não seja a solução nula.  
Ou seja, queremos a solução na forma
$$u(x,t) = X(x)T(t)$$
Primeiro nós veremos a condição de condições de contorno.

Como queremos que u(x,t) satisfaça 3, nós temos
$$u(0,t) = X(0)T(t) = 0 \qquad (4)$$
$$u(L,t) = X(L)T(t) = 0 \qquad (5)$$
Como não queremos a solução nula, nós precisamos assumir que \\(T(t)\\) não seja 0, pois se não \\(u(x,t) = 0\\) para todo \\(t\\).

Então daqui temos que 
$$X(0) = X(L) = 0$$
Com isso nós conseguimos partir para (1), que nos da:
$$X(x)\dot{T}(t) = a^2\ddot{X}(x)T(t) \Rightarrow$$
$$\frac{\dot{T}(t)}{T(t)} = a^2\frac{\ddot{X}(x)}{X(x)} = \lambda$$
E com isso temos o sistema

$$\begin{dcases}\frac{\dot{T}(t)}{T(t)} = \lambda \cr \cr a^2\frac{\ddot{X}(x)}{X(x)}=\lambda\end{dcases}$$

Para resolvermos a segunda parte do sistema é apenas resolver uma EDO de segunda ordem, com as condições (4) e (5).

Nós temos que estar para todo caso de \\(\lambda\\) ou seja, se é menor que 0, maior que 0 ou 0 mesmo.  
Mas o único caso que funciona é o caso em que \\(\lambda<0\\).

Com isso achamos a solução para \\(X(x)\\):
$$X(x) = c_1\sin\left(\frac{\sqrt{-\lambda}}{a}x\right) \qquad c_1\in \R$$
Veja bem, se aplicarmos essa função na condição (4), nós chegaremos que \\(c_1\\) deve ser 0 ou \\(\sin\left(\frac{\sqrt{-\lambda}}{a}L\right)\\) deve ser 0, se \\(c_1\\) for 0 teremos a solução nula e não queremos isso, então temos que partir para outra abordagem, com isso temos:
$$\sin\left(\frac{\sqrt{-\lambda}}{a}L\right) = 0 \iff \frac{\sqrt{-\lambda}}{a}L = n\pi \iff$$
$$\iff \lambda = -\frac{n^2\pi^2a^2}{L^2}$$

E continuaremos a falar sobre a solução no próximo post.