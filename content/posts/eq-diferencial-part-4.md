---
title: "1.1.2- Equação do Calor - Parte 3"
date: 2022-09-23T16:32:10-03:00
draft: false
katex: true
markup: 'mmark'
---

Nós gostariamos de saber se existe uma solução para nosso problema do calor, porém agora para uma condição inicial arbitrária.  
Mas pra isso, primeiro nós precisamos abordar outro assunto:
### Série de Fourier
Uma série de Fourier, é uma maneira de representar funções de período \\(2L\\) por meio de uma série trigonométrica.

Então dada uma função \\(f\\) que satisfaz as condições:
- A função é injetora e contínua exceto em um número finito de pontos no período \\(2L\\).
- A função tem um número finito de máximos e mínimos dentro do período;
- A função é absolutamente integrável.

Então a série de Fourier de f é dada por
$$S(x) = \frac{a_0}{2}+\sum_{n=1}^{\infty}\left[a_n\cos\left(\frac{n\pi x}{L}\right)+ b_n\sin\left(\frac{n \pi x}{L}\right)\right]$$
Onde
$$ a_0 = \frac{1}{L}\int_{-L}^{L}f(x)dx \quad a_n = \frac{1}{L}\int_{-L}^{L}f(x)\cos\left(\frac{n \pi x}{L}\right)\quad b_n = \frac{1}{L}\int_{-L}^{L}f(x)\sin\left(\frac{n \pi x}{L}\right)dx$$
Para \\(n\geq 1\\).

Agora nós podemos voltar a falar da equação do calor.  
Como nós dissemos antes, nós gostariamos de aplicar nossa solução geral a qualquer condição inicial \\(u_0(x)\\), porém primeiro nós vamos voltar a falar da solução.

Como nós vimos antes, soma de soluções também é uma solução da equação diferencial, então, ao invés de somarmos apenas \\(N\\) soluções, por que não somarmos infinitas?  
Ou seja
$$u(x,t) =\sum_{n=1}^{\infty} \alpha_n e^{-\frac{n^2\pi^2a^2}{L^2} t}\sin\left( \frac{n\pi}{L} x \right)$$
> Matemáticamente não é nada simples somarmos infinitos termos, pois nem sempre as séries convergem e é um pouco difícil de trabalhar com isso, porém aqui não falaremos da parte mais técnica e teórica e seguiremos com a equação do calor.
Aqui nós já conseguimos ver onde nós queremos chegar.

Como nós queremos a solução para qualquer condição inicial, nós queremos um jeito de podermos escrever \\(u_0(x)\\) como
$$u_0(x) =\sum_{n=1}^{\infty} \alpha_n \sin\left( \frac{n\pi}{L} x \right)$$

E como nós vimos, nós conseguimos representar uma função como uma série de senos, como uma série de Fourier, porém ainda há algumas condições que precisamos satisfazer.

Queremos que \\(u_0(x)\\) seja descontínua num número finitos de pontos e também, precisamo que ela seja \\(2L\\) periódica.

A primeira trivial de resolver, já a segunda também, bastamos estendermos ela em uma função *ímpar* de período \\(2L\\).

> Aqui nós faremos uma extensão ímpar, pois nós extamos representando nossa função como uma soma de senos, que são funções ímpares.
Então chamo de \\(\tilde{u}_0(x)\\) essa extenção, então temos que
$$\tilde{u}_0(-x) = -\tilde{u}_0(x) \qquad \tilde{u}_0(x) = \tilde{u}_0(x+2L)$$
Como isso dado uma função \\(u_0(x)\\) qualquer podemos fazer essa extençao e conseguimos fazer uma série de Fourier de \\(\tilde{u}_0(x)\\) tal que
$$\tilde{u}_0(x) = \sum _ {n=1}^{\infty}\alpha_n \sin\left( \frac{n\pi}{L} x \right)$$
Onde 
$$\alpha_n = \frac{1}{L}\int _ {-L}^{L}\tilde{u}_0(x)\sin\left(\frac{n \pi x}{L}\right)$$

Então temos que nossa solução geral de 
$$\begin{dcases}
\frac{\partial u}{\partial t} =  a^2 \frac{\partial^2 u}{\partial x^2}  &(1)\cr
u(x,0) = u_0(x)  &(2) \cr
u(0,t) = u(L,t) = 0 & (3)
\end{dcases}$$
é
$$u(x,t) =\sum_{n=1}^{\infty} \alpha_n e^{-\frac{n^2\pi^2a^2}{L^2} t}\sin\left( \frac{n\pi}{L} x \right)$$
Com os \\(\alpha_n\\) já apresentados.