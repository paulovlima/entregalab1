---
title: "1.1.1- Equação do Calor - Parte 2"
date: 2022-09-23T16:32:10-03:00
draft: false
katex: true
markup: 'mmark'
---
Como nós vimos anteriormente, para que não obtivéssemos a equação identicamente nula nós precisamos assumir que
$$\lambda = -\frac{n^2\pi^2a^2}{L^2}$$
Com isso chegamos que
$$X(x) =\left( \frac{\sqrt{-\lambda}}{a} x \right) =c_1\sin\left( \frac{n\pi}{L} x \right)$$
Como essa equação vale para todo \\(n\\) natural. É rasoável que nós indetifiquemos nossa equação.

Ou seja, a n-ésima solução é

$$X_n(x) = c_n\sin\left( \frac{n\pi}{L} x \right)$$
Agora voltando ao nosso sistema, nós tinhamos que
$$\frac{\dot{T}(t)}{T(t)} = \lambda$$
Resolvendo a equação diferencial, chegamos em
$$T(t) = b_1e^{\lambda t} = b_1e^{-\frac{n^2\pi^2a^2}{L^2} t}$$

Então nós achamos nossa função \\(u(x,t)\\)
$$u_n(x,t) = T(t)X_n(x) = \alpha_n e^{-\frac{n^2\pi^2a^2}{L^2} t}\sin\left( \frac{n\pi}{L} x \right)$$

Agora que temos nossa n-ésima solução, nós podemos tentar achar uma solução geral.

Nós sabemos que a soma de soluções de uma equação diferencial também é uma solução.  
Então, é interessante que coloquemos nossa solução geral como
$$u(x,t) = \sum_{n=1}^{N}u_n(x,t) \Rightarrow$$
$$u(x,t) =\sum_{n=1}^{N} \alpha_n e^{-\frac{n^2\pi^2a^2}{L^2} t}\sin\left( \frac{n\pi}{L} x \right)$$

Então chegamos na nossa solução, e note que
$$u_0(x) =u(x,0) =  \sum_{n=1}^{N} \alpha_n\sin\left( \frac{n\pi}{L} x \right)$$

Então, se tiver um problema 
$$\begin{dcases}
\frac{\partial u}{\partial t} =  a^2 \frac{\partial^2 u}{\partial x^2}  &(1)\cr
u(x,0) = \sum_{n=1}^{N} \alpha_n\sin\left( \frac{n\pi}{L} x \right)  &(2) \cr
u(0,t) = u(L,t) = 0 & (3)
\end{dcases}$$
Nós temos que a solução geral é dada por
$$u(x,t) =\sum_{n=1}^{N} \alpha_n e^{-\frac{n^2\pi^2a^2}{L^2} t}\sin\left( \frac{n\pi}{L} x \right)$$

Porém, e se não tiver um \\(u_0(x)\\) do jeito apresentado?  
Ele pode ser alguma outra função?  
Nós abordaremos isso num próximo post.