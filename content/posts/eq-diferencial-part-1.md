---
title: "1.0- Equação Diferencial Parcial"
date: 2022-09-23T16:32:10-03:00
draft: false
katex: true
markup: 'mmark'
---
Equações diferencias estão em todo o lugar.  
Diversos fenômenos de física são descrito por equações diferencias.  
Um problema especial das equações diferencias são as equações diferências parciais.  
Ou seja um equação diferencial que está sendo diferenciada em mais de uma variável.

Vamos a alguns exemplos pra ficar mais claro:

## Equação do Calor
A equação do calor é dado da seguinte maneira:

Suponha que temos uma barra metálica que possui o tamanho **L** a equação da difusão do calor na barra é dada por:
$$ \frac{\partial u}{\partial t} =  a^2 \frac{\partial^2 u}{\partial x^2} $$
Onde \\(u(x,t)\\) é a dita a temperatura no ponto \\(x\\) no instante \\(t\\) na barra.

### C.I -  Condição Inicial
$$ u(x,0) = u_0(x) $$
### C.C - Condição de contorno
$$u(0,t) = 0 \qquad \qquad u(L,t) = 0  $$

## Equação da Onda na Corda
Da mesma maneira da equação do calor, nós vamos supor agora uma corda de comprimento **L**, então a equação da onde é:
$$\frac{\partial^2 u}{\partial t^2} = a^2\frac{\partial^2 u}{\partial x^2}$$
Onde \\(u(x,t)\\) é a posição do ponto \\(x\\) da corda no instante \\(t\\)

### C.I
$$ u(x,0) = u_0(x)$$
$$ \frac{\partial u}{\partial t}(x,0) = v_0(x)$$
### C.C
$$u(0,t) = 0$$
$$u(L,t) = 0$$


Esses são alguns exemplos de onde usamos a equação diferencial parcial, porém na próxima sessão falaremos mais sobre nosso primeiro exemplo.
