---
title: "About"
date: 2022-09-23T16:32:10-03:00
draft: false
katex: true
markup: 'mmark'
---
# Esta página contém informações sobre mim!
Olá, meu nome é Paulo Vieira, sou estudante de Matemática Aplica pelo IME-USP e esse é um blog para eu ir colocando os conteúdos de algumas aulas para que nós possamos aprender juntos.
> "Quem ensina aprende ao ensinar e quem aprende ensina ao aprender"

>                                                           Paulo Freire